package com.yourcompany.model.reservation;

import java.math.BigDecimal;

public class Reservation {
	private boolean upgradeMeal;
	private BigDecimal payment;
	private Customer customer;

	public boolean isUpgradeMeal() {
		return upgradeMeal;
	}

	public void setUpgradeMeal(boolean upgradeMeal) {
		this.upgradeMeal = upgradeMeal;
	}

	public BigDecimal getPayment() {
		return payment;
	}

	public void setPayment(BigDecimal payment) {
		this.payment = payment;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Reservation [upgradeMeal=").append(upgradeMeal).append(", payment=").append(payment)
				.append(", customer=").append(customer).append("]");
		return builder.toString();
	}

}
