package com.yourcompany.model.reservation;

public class Customer {
	private String name;
	private String loyaltyLevel;

	public Customer() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Customer(String name, String loyaltyLevel) {
		super();
		this.name = name;
		this.loyaltyLevel = loyaltyLevel;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLoyaltyLevel() {
		return loyaltyLevel;
	}

	public void setLoyaltyLevel(String loyaltyLevel) {
		this.loyaltyLevel = loyaltyLevel;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Customer [name=").append(name).append(", loyaltyLevel=").append(loyaltyLevel).append("]");
		return builder.toString();
	}
	
	

}
