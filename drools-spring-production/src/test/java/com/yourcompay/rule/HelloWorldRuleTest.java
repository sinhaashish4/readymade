package com.yourcompay.rule;

import org.junit.Test;

import com.yourcompany.decisiontable.Driver;
import com.yourcompany.model.Message;
import com.yourcompany.service.RuleService;

public class HelloWorldRuleTest extends RuleTest {
	@Test
	public void runRule() {
		Message message = new Message();
		message.setMessage("Hello World");
		message.setStatus(Message.HI);
		ruleService.execute(RuleService.KSESSION.KSESSION_MESSAGE, message);
	}

	@Test
	public void run_Decision_Table() {
		Driver driver = new Driver();
		driver.setAge(new Integer(20));
		driver.setAccident(new Integer(0));
		ruleService.execute_Stateless(RuleService.KSESSION.DTKS, driver);
	}

}
