package com.yourcompay.rule.accumulate;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.ObjectFilter;

import com.yourcompany.model.accumulate.Order;
import com.yourcompany.model.accumulate.OrderItem;
import com.yourcompany.model.accumulate.Vourcher;
import com.yourcompany.service.RuleService;
import com.yourcompay.rule.RuleTest;

public class DiscountTest extends RuleTest {

	@Test
	public void accumulate() {
		Order order = new Order("Ash-order");
		OrderItem item1 = new OrderItem(order, "Yogurt", 3, 5.00d);
		OrderItem item2 = new OrderItem(order, "Honey", 1, 15.00d);
		OrderItem item3 = new OrderItem(order, "Mango", 4, 1.00d);
		OrderItem item4 = new OrderItem(order, "Eggs", 10, 0.50d);

		List<OrderItem> items = Arrays.asList(new OrderItem[] { item1, item2, item3, item4 });

		order.getItems().addAll(items);

		KieSession session = ruleService.execute(RuleService.KSESSION.ACCUMULATEKS, order);

		double orderAmount = 39.0d;

		Assert.assertTrue("Amount not reflected into order.", orderAmount == order.getAmount());

		int discount = 10;
		Assert.assertTrue("Discount not given for order more than 20.", discount == order.getDiscount());

		for (Object obj : session.getObjects()) {
			System.out.println(obj.getClass());
		}

		Collection<Vourcher> vouchers = findall(session, Vourcher.class);
		Vourcher v1 = vouchers.iterator().next();

		Assert.assertTrue("There is one parking voucher.", v1 != null);

		session.dispose();

	}

	public <T> Collection<T> findall(KieSession session, Class<T> clazz) {
		@SuppressWarnings("unchecked")
		Collection<T> items = (Collection<T>) session.getObjects(new ObjectFilter() {
			@Override
			public boolean accept(Object object) {
				return clazz.isAssignableFrom(object.getClass());
			}
		});
		return items;
	}
}
