package com.yourcompay.rule.accumulate;

import java.util.Arrays;

import org.junit.Test;

import com.yourcompany.model.accumulate.Reading;
import com.yourcompany.model.accumulate.Sensor;
import com.yourcompany.service.RuleService;
import com.yourcompay.rule.RuleTest;;

public class SensorTest extends RuleTest {

	@Test
	public void accumulate_1() {
		Sensor sensor = new Sensor("Sensor-1");
		Reading reading1 = new Reading("1_Reading", sensor, 10);
		Reading reading2 = new Reading("2_Reading", sensor, 20);
		Reading reading3 = new Reading("3_Reading", sensor, 30);
		Reading reading4 = new Reading("4_Reading", sensor, 40);

		Reading[] array = new Reading[] { reading1, reading2, reading3, reading4 };

		sensor.getReadings().addAll(Arrays.asList(array));

		ruleService.execute(RuleService.KSESSION.ACCUMULATEKS, sensor);

	}

}
