package com.yourcompay.rule;

import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.yourcompany.service.RuleContainer;
import com.yourcompany.service.RuleService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = RuleContainer.class)
public abstract class RuleTest {

	@Autowired
	public RuleService ruleService;

}
