package com.yourcompay.rule.store;

import org.junit.Test;

import com.yourcompany.model.store.Customer;
import com.yourcompany.model.store.Order;
import com.yourcompany.model.store.OrderItem;
import com.yourcompany.service.RuleService;
import com.yourcompay.rule.RuleTest;

public class StoreOrderTest extends RuleTest {
	
	@Test
	public void orderOutOfStockTest() {
		
		//Build Orders for customer Ash
		String customerName = "Ash";
		Customer customer = new Customer();
		customer.setName(customerName);
		
		//Build order1 for Ash
		Order order1 = new Order("Desert Order");
		order1.setCustomer(customer);

		//Add items for order 1
		OrderItem orderItem11 = new OrderItem();
		orderItem11.setName("Yougurt");
		//Yogurt sold out
		orderItem11.setSold(true);
		orderItem11.setOrder(order1);
		
		order1.addItem(orderItem11);
		
		OrderItem orderItem12 = new OrderItem();
		orderItem12.setName("Strawbery");
		//Strawbery is sold out
		orderItem12.setSold(true);
		orderItem12.setOrder(order1);
		
		order1.addItem(orderItem12);
		
		
		//Build order 2 for Ash
		Order order2 = new Order("Hot food order");
		order2.setCustomer(customer);
		
		//Add item 1 for order 2
		OrderItem orderItem21 = new OrderItem();
		orderItem21.setName("Panini");
		orderItem21.setOrder(order2);
		
		order2.addItem(orderItem21);
		
		//Add item 2 for order 2
		OrderItem orderItem22 = new OrderItem();
		orderItem22.setName("water");
		orderItem22.setOrder(order2);
		
		order2.addItem(orderItem22);
		
		ruleService.execute(RuleService.KSESSION.STOREKS, order1, order2);
		
	}
}
