package com.yourcompany.decisiontable;

public class Driver {
	private Integer age;
	private Integer accident;

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public Integer getAccident() {
		return accident;
	}

	public void setAccident(Integer accident) {
		this.accident = accident;
	}

}
