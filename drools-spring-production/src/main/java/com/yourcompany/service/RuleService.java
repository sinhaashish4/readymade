package com.yourcompany.service;

import java.util.Arrays;

import org.kie.api.event.rule.DebugAgendaEventListener;
import org.kie.api.event.rule.DebugRuleRuntimeEventListener;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.StatelessKieSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RuleService {

	@Autowired
	private KieContainer kieContainer;

	public enum KSESSION {
		KSESSION_MESSAGE, DTKS, STOREKS, ACCUMULATEKS
	};

	// KieSession is statefulSession
	// dispose method must be called after fireAllRules()
	public KieSession execute(RuleService.KSESSION session_name, Object... ojbects) {
		KieSession ksession = kieContainer.newKieSession(session_name.name());

		// Debug listener
		ksession.addEventListener(new DebugAgendaEventListener());
		ksession.addEventListener(new DebugRuleRuntimeEventListener());

		for (Object object : ojbects)
			ksession.insert(object);

		// fire the rules
		ksession.fireAllRules();

		// dispose the session
		// ksession.dispose();

		return ksession;
	}

	// StatelessKieSession - does not re-run rules if fact gets modified during
	// execution.
	public void execute_Stateless(RuleService.KSESSION session_name, Object ojbect) {
		StatelessKieSession ksession = kieContainer.newStatelessKieSession(session_name.name());
		ksession.execute(Arrays.asList(new Object[] { ojbect }));
	}

}
