package com.yourcompany.model.accumulate;

public class Reading {
	private String name;
	private Sensor senser;
	private int temperature;

	public Reading() {
		super();
	}

	public Reading(String name, Sensor senser, int temperature) {
		super();
		this.name = name;
		this.senser = senser;
		this.temperature = temperature;
	}

	public String getName() {
		return name;
	}

	public Sensor getSenser() {
		return senser;
	}

	public int getTemperature() {
		return temperature;
	}

}
