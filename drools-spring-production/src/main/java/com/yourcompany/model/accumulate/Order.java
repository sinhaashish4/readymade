package com.yourcompany.model.accumulate;

import java.util.ArrayList;
import java.util.List;

public class Order {

	private String name;
	private List<OrderItem> items = new ArrayList<OrderItem>();
	private int discount;
	private double amount;

	public Order(String name) {
		super();
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public List<OrderItem> getItems() {
		return items;
	}

	public int getDiscount() {
		return discount;
	}

	public void setDiscount(int discount) {
		this.discount = discount;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}
}
