package com.yourcompany.model.accumulate;

import java.util.ArrayList;
import java.util.List;

public class Sensor {
	private String name;

	private List<Reading> readings = new ArrayList<Reading>();

	public Sensor(String name) {
		super();
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public List<Reading> getReadings() {
		return readings;
	}
}
