package com.yourcompany.model.accumulate;

public class OrderItem {
	private Order order;
	private String name;
	private int quantity;
	private double price;

	public OrderItem() {
		super();
	}

	public OrderItem(Order order, String name, int quantity, double price) {
		super();
		this.order = order;
		this.name = name;
		this.quantity = quantity;
		this.price = price;
	}

	public Order getOrder() {
		return order;
	}

	public String getName() {
		return name;
	}

	public int getQuantity() {
		return quantity;
	}

	public double getPrice() {
		return price;
	}

}
