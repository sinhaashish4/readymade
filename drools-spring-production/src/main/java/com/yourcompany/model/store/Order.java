package com.yourcompany.model.store;

import java.util.ArrayList;
import java.util.List;

public class Order {
	private String name;
	private Customer customer;
	private List<OrderItem> items = new ArrayList<OrderItem>();
	private boolean outOfOrder;

	public Order(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void addItem(OrderItem item) {
		items.add(item);
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public boolean isOutOfOrder() {
		return outOfOrder;
	}

	public void setOutOfOrder(boolean outOfOrder) {
		this.outOfOrder = outOfOrder;
	}

	public List<OrderItem> getItems() {
		return items;
	}

}
