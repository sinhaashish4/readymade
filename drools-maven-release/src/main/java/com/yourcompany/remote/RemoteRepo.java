package com.yourcompany.remote;

import java.util.ArrayList;
import java.util.List;

import org.kie.api.KieServices;
import org.kie.api.builder.KieScanner;
import org.kie.api.builder.ReleaseId;
import org.kie.api.command.Command;
import org.kie.api.runtime.ExecutionResults;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.StatelessKieSession;
import org.kie.internal.command.CommandFactory;

import com.redhat.demos.dm.loan.model.Applicant;
import com.redhat.demos.dm.loan.model.Loan;

public class RemoteRepo {
	private static final int CREDIT_SCORE = 230;
	private static String SETTINGS = "/Users/ash/Documents/STUDY/Redhat/BRMS-Code/ash/readymade-solution/drools-maven-release/src/main/resources/settings.xml";
	private static String GROUP_ID = "com.redhat.demos.dm";
	private static String ARTIFACT_ID = "loan-application";
	private static String VERSION = "LATEST";

	public static void main(String[] arr) {
		StatelessKieSession ksession = rule_engine();

		List<Command<?>> cmds = build_data();

		// Execute the list
		ExecutionResults results = ksession.execute(CommandFactory.newBatchExecution(cmds));

		System.out.println(results.getValue("loan"));

	}

	private static List<Command<?>> build_data() {
		// Build applicant
		Applicant applicant = new Applicant();
		applicant.setName("Ash");
		applicant.setCreditScore(CREDIT_SCORE);

		// Build loan
		Loan loan = new Loan();
		loan.setAmount(2500);
		loan.setDuration(24);
		loan.setApproved(false);
		loan.setInterestRate(1.5d);

		List<Command<?>> cmds = new ArrayList<Command<?>>();
		cmds.add(CommandFactory.newInsert(applicant, "ash"));
		cmds.add(CommandFactory.newInsert(loan, "loan"));

		return cmds;
	}

	private static StatelessKieSession rule_engine() {
		System.setProperty("kie.maven.settings.custom", SETTINGS);

		KieServices services = KieServices.Factory.get();
		ReleaseId id = services.newReleaseId(GROUP_ID, ARTIFACT_ID, VERSION);
		KieContainer container = services.newKieContainer(id);

		// Add scanner
		KieScanner kScanner = services.newKieScanner(container);
		kScanner.scanNow();

		String name = "default-stateless-ksession";
		StatelessKieSession ksession = container.newStatelessKieSession(name);

		System.out.println("KSession found: " + ksession);
		return ksession;
	}
}
