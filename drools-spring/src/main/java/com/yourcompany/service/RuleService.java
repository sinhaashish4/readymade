package com.yourcompany.service;

import org.kie.api.event.rule.DebugAgendaEventListener;
import org.kie.api.event.rule.DebugRuleRuntimeEventListener;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RuleService {

	@Autowired
	private KieContainer kieContainer;

	public enum KSESSION {
		KSESSION_MESSAGE
	};

	public void execute(RuleService.KSESSION session_name, Object ojbect) {
		KieSession ksession = kieContainer.newKieSession(session_name.name());

		// Debug listener
		ksession.addEventListener(new DebugAgendaEventListener());
		ksession.addEventListener(new DebugRuleRuntimeEventListener());

		ksession.insert(ojbect);

		// fire the rules
		ksession.fireAllRules();

		// dispose the session
		ksession.dispose();
	}
}
