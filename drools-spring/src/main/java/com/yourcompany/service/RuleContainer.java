package com.yourcompany.service;

import org.kie.api.KieServices;
import org.kie.api.runtime.KieContainer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("com.yourcompany")
public class RuleContainer {

	@Bean
	public KieContainer getKContainer() {
		KieServices ks = KieServices.get();
		return ks.getKieClasspathContainer();
	}

}
