package com.yourcompay.rule;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.yourcompany.model.Message;
import com.yourcompany.service.RuleContainer;
import com.yourcompany.service.RuleService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = RuleContainer.class)
public class RuleTest {

	@Autowired
	private RuleService ruleService;

	@Test
	public void runRule() {
		Message message = new Message();
		message.setMessage("Hello World");
		message.setStatus(Message.HI);
		ruleService.execute(RuleService.KSESSION.KSESSION_MESSAGE, message);
	}

}
