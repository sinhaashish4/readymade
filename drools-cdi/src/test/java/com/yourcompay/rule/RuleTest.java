package com.yourcompay.rule;

import javax.inject.Inject;

import org.jboss.weld.junit4.WeldInitiator;
import org.junit.Rule;
import org.junit.Test;

import com.yourcompany.model.Message;
import com.yourcompany.service.RuleContainer;
import com.yourcompany.service.RuleService;

public class RuleTest {
	
	@Rule
	public WeldInitiator weld = WeldInitiator.from(RuleService.class, RuleContainer.class).inject(this).build();

	@Inject
	private RuleService ruleService;

	@Test
	public void runRule() {
		Message message = new Message();
		message.setMessage("Hello World");
		message.setStatus(Message.HI);
		ruleService.execute(RuleService.KSESSION.KSESSION_MESSAGE, message);
	}

}
