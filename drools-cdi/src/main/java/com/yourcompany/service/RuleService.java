package com.yourcompany.service;

import javax.inject.Inject;

import org.kie.api.event.rule.DebugAgendaEventListener;
import org.kie.api.event.rule.DebugRuleRuntimeEventListener;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;

public class RuleService {

	@Inject
	private KieContainer container;

	public enum KSESSION {
		KSESSION_MESSAGE
	};

	public void execute(RuleService.KSESSION session_name, Object ojbect) {
		KieSession ksession = container.newKieSession(session_name.name());

		// Debug listener
		ksession.addEventListener(new DebugAgendaEventListener());
		ksession.addEventListener(new DebugRuleRuntimeEventListener());

		ksession.insert(ojbect);

		// fire the rules
		ksession.fireAllRules();

		// dispose the session
		ksession.dispose();
	}
}
