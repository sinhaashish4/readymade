package com.yourcompany.service;

import javax.enterprise.inject.Produces;

import org.kie.api.KieServices;
import org.kie.api.runtime.KieContainer;

public class RuleContainer {

	@Produces
	public KieContainer getKContainer() {
		KieServices ks = KieServices.get();
		return ks.getKieClasspathContainer();
	}
}
