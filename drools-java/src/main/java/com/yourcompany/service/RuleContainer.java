package com.yourcompany.service;

import org.kie.api.KieServices;
import org.kie.api.runtime.KieContainer;

public class RuleContainer {

	private static KieContainer kContainer = null;

	private RuleContainer() {
	}

	public static KieContainer getKContainer() {
		if (kContainer == null)
			synchronized (RuleContainer.class) {
				if (kContainer == null) {
					KieServices ks = KieServices.get();
					kContainer = ks.getKieClasspathContainer();
				}
			}
		return kContainer;
	}
}
