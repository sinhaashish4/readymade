package com.yourcompany.service;

import org.kie.api.event.rule.DebugAgendaEventListener;
import org.kie.api.event.rule.DebugRuleRuntimeEventListener;
import org.kie.api.runtime.KieSession;

public class RuleService {

	public enum KSESSION {
		KSESSION_MESSAGE
	};

	public void execute(RuleService.KSESSION session_name, Object ojbect) {
		KieSession ksession = RuleContainer.getKContainer().newKieSession(session_name.name());

		// Debug listener
		ksession.addEventListener(new DebugAgendaEventListener());
		ksession.addEventListener(new DebugRuleRuntimeEventListener());

		ksession.insert(ojbect);

		// fire the rules
		ksession.fireAllRules();

		// dispose the session
		ksession.dispose();
	}
}
