package com.yourcompay.rule;

import org.junit.Test;

import com.yourcompany.model.Message;
import com.yourcompany.service.RuleService;

public class RuleTest {

	@Test
	public void runRule() {
		RuleService ruleService = new RuleService();

		Message message = new Message();
		message.setMessage("Hello World");
		message.setStatus(Message.HI);

		ruleService.execute(RuleService.KSESSION.KSESSION_MESSAGE, message);

	}

}
